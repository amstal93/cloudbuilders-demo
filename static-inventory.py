#!/usr/bin/env python3
import json

def main():
    inventory = json.loads(open('inventory').read())
    print(json.dumps(inventory))

if __name__ == "__main__":
    main()
