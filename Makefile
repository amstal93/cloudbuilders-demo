# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

install-depends:  ## Install python dependencies
	@echo 'installing python dependencies'
	pip3 install --user --upgrade docker docker-compose netaddr requests paramiko
	@echo 'Please ensure you have installed ansible via your favorite package manager'

setup:
	@echo 'Preparing docker environment dependencies'
	ansible-playbook playbooks/01-setup.yaml

start: setup  ## Bootstrap the docker environment and prepare for ansible playbooks to be run against containers
	@echo 'Starting bootstrap of docker environment'
	ansible-playbook -i static-inventory.py playbooks/02-bootstrap.yaml

configure:  ## Execute ansible to configure cEOS containers
	@echo 'Configuring docker environment'
	ansible-playbook -i static-inventory.py playbooks/04-ceos_config_push.yaml

get-facts:  ## Tests SSH and user roles by collecting priv data
	@echo 'Collecting config and hardwere facts'
	ansible-playbook -i static-inventory.py playbooks/06-test-cli-role.yaml

destroy:  ## Destroys the docker environment
	@echo 'Destroying docker environment'
	ansible-playbook -i static-inventory.py playbooks/05-destroy.yaml

recreate: destroy start  ## Destory and bootstrap a clean environment

running:  ## Display running containers
	docker-compose ps

refresh-inventory:  ## Recreates ansible-inventory file
	@echo 'Recreating ansible-inventory file'
	ansible-inventory -i docker-inventory.py --list > inventory
	cat inventory | head -n 20

clean:  ## Cleanup the environment, remove all images, excludes cEOS base image.
	docker image rm $$(docker image ls | grep -v REPO | grep c99 | awk '{print $$3}') -f
	docker image ls
	docker ps -a
